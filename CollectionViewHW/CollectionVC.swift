//  ViewController.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import UIKit

class CollectionVC: UIViewController {
    
    @IBOutlet weak var collOutlet: UICollectionView!
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let itemsPerRow = 3
    let margin: CGFloat = 5
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "detailSegue" else { return }
        
        if let cell = sender as? UICollectionViewCell,
           let indexPath = collOutlet.indexPath(for: cell),
           let vc = segue.destination as? DetailVC {
            
            if indexPath.section == 0 {
                vc.informDetail = PhotoStorage.itemCat(forIndex: indexPath.item)
            } else {
                vc.informDetail = PhotoStorage.itemDog(forIndex: indexPath.item)
            }
        }
    }
}


extension CollectionVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let item = PhotoStorage.itemCat(forIndex: indexPath.item)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
            cell.imageCell.image = UIImage(named: item.imageName)
            // print(indexPath)
            return cell
            
        } else {
            let item = PhotoStorage.itemDog(forIndex: indexPath.item)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
            cell.imageCell.image = UIImage(named: item.imageName)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionCollectionReusableView", for: indexPath) as! SectionCollectionReusableView
        
        if indexPath.section == 0 {
            header.sectionNameLable.text = "Cats"
        } else {
            header.sectionNameLable.text = "Dogs"
        }
        return header
    }
    
}


extension CollectionVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, canEditItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}


extension CollectionVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (UIScreen.main.bounds.width - CGFloat(itemsPerRow + 1) * margin) / CGFloat(itemsPerRow)
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 15, left: 0, bottom: margin, right: 0)
    }
    
}


