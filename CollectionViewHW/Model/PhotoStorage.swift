//  PhotoStorage.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import Foundation
import UIKit

struct PhotoStorage {
    
    static let photoCats: Array<Photo> = [
        Photo(imageName: "cat1", name: "Вася", category: "Cats", data: "20/01/2020", camera: "telephone cam"),
        Photo(imageName: "cat2", name: "Мурчик", category: "Cats", data: "20/01/2020", camera: "pc cam"),
        Photo(imageName: "cat3", name: "Маня", category: "Cats", data: "20/01/2020", camera: "telephone cam")
    ]
    
    static let photoDogs: Array<Photo> = [
        Photo(imageName: "dog1", name: "Герда", category: "Dogs", data: "20/01/2020", camera: "telephone cam"),
        Photo(imageName: "dog2", name: "Багира", category: "Dogs", data: "20/01/2020", camera: "pc cam"),
        Photo(imageName: "dog3", name: "Багги", category: "Dogs", data: "20/01/2020", camera: "telephone cam")
    ]
    
    static func itemCat(forIndex index: Int) -> Photo {
        return photoCats[index % photoCats.count]
    }
    
    static func itemDog(forIndex index: Int) -> Photo {
        return photoDogs[index % photoDogs.count]
    }
    
}
