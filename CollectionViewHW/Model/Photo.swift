//  Photo.swift
//  CollectionViewHW
//  Created by Viktoriia Skvarko on 10.02.2021.


import Foundation
import UIKit

struct Photo {
    var imageName: String
    var name: String
    var category: String
    var data: String
    var camera: String
}
